import java.util.Scanner;
/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 29.08.2017
  * @author Carsten Miericke 
  */

public class Date290817 {
  
  public static void main(String[] args) {
    /**
    // Dekleration: speicher  wird für Variable mit Datentyp reserviert
    // Initialisierung Wert wird einer Variable zugewiesen
    double preis_o_Mwst=10;            //basis Preis, komma
    double preis_m_Mwst = preis_o_Mwst * 1.19;
    System.out.println(preis_o_Mwst);
    System.out.println(preis_m_Mwst);
    
    String name;
    int alter;
    double groesse;
    //scanner
    Scanner scan = new Scanner(System.in);
    System.out.println("Wie heißt du?");
    name = scan.next() ;
    System.out.println("Hallo " + name + " wie alt bist du?");
    alter = scan.nextInt();
    System.out.println("Schön. Und wie groß bist du?");
    groesse = scan.nextInt();
    System.out.println("Ok,  du heißt " + name + ", bist " + alter + " Jahre alt und " + groesse + "m groß");
    **/
    //Afugabe 1
    
    String vorname, nachname, strasse, ort, land;
    int hausnummer, plz;
    Scanner sc = new Scanner(System.in);
    
    System.out.println("Vorname:");
    vorname = sc.next() ;
    System.out.println("Nachname:");
    nachname = sc.next();
      System.out.println("Strasse:");
      strasse = sc.next();
      System.out.println("Hausnummer:");
      hausnummer = sc.nextInt();
      System.out.println("PLZ:");
      plz = sc.nextInt();
      System.out.println("Ort:");
      ort = sc.next();
      System.out.println("Land:");
      land = sc.next();
      System.out.println("* edv-Tech GmbH * Sonnenweg 12 * 12345 Mondstadt ");
      System.out.println("\t" + vorname + " " + nachname);
      System.out.println("\t" + strasse + " "+ hausnummer);
      System.out.println("\t" + plz + " " + ort);
      System.out.println("\t" + land);

// Aufgabe 2
String kurs;
int fehltage;
double note;

      System.out.println("Vorname:");
      vorname = sc.next();
      System.out.println("Nachname:");
      nachname = sc.next();
      System.out.println("kurs");
      kurs = sc.next();
      System.out.println("Note:");
      note = sc.nextDouble();
      System.out.println("Fehltage:");
      fehltage = sc.nextInt();
      
      System.out.println("Der Schüler " + vorname + " " +nachname + " hat im Kurs " +kurs + " die Note: " + note + "erzielt. dabei hatte er " + fehltage + " Fehltage.");
  } // end of main

} // end of class Date290817
